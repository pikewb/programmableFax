import React, { Component } from 'react';
import Paper from 'material-ui/Paper';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import CircularProgress from 'material-ui/CircularProgress';
import Dialog from 'material-ui/Dialog';
import { red500 } from 'material-ui/styles/colors';
import base64 from 'base-64';

const baseUrl = 'https://fax.twilio.com/v1/Faxes';

export default class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      sending: false,
      To: '',
      From: '',
      MediaUrl: '',
      AccountSID: '',
      Token: '',
      open: false,
      status: false
    };
  }

  getHeaders() {
    const headers = new Headers();
    const authString = `${this.state.AccountSID}:${this.state.Token}`;
    const authToken = base64.encode(authString);
    headers.append('Authorization', `Basic ${authToken}`);
    return headers;
  }

  getStatus() {
    const headers = this.getHeaders();
    fetch(`${baseUrl}/${this.state.faxSid}`, { headers })
    .then((res) => res.json()).then((res) => {
      this.setState({ status: res.status });
      if (res.status === 'sending' || res.status === 'queued' || res.status === 'processing') {
        setTimeout(() => this.getStatus(), 10000);
      } else {
        setTimeout(() => this.setState({ status: false }), 5000);
      }
      return true;
    }).catch();
  }

  post() {
    const headers = this.getHeaders();
    const formData = new FormData();
    formData.append('To', this.state.To);
    formData.append('From', this.state.From);
    formData.append('MediaUrl', this.state.MediaUrl);
    const params = {
      method: 'POST',
      headers,
      body: formData
    };
    this.setState({ open: false });
    fetch(baseUrl, params).then((res) => res.json())
    .then((body) => {
      this.setState({
        faxSid: body.sid,
        status: body.status
      });
      setTimeout(() => this.getStatus(), 1000);

      return true;
    }).catch();
  }

  submit = () => {
    this.setState({ open: true });
  }

  handleClose = () => {
    this.setState({ open: false });
  }

  updateField = (fieldName, event) => {
    this.setState({ [fieldName]: event.target.value });
  }

  render() {
    const actions = [
      <RaisedButton
        label="キャンセル"
        onTouchTap={this.handleClose.bind(this)}
      />,
      <RaisedButton
        label="送信"
        primary
        onTouchTap={this.post.bind(this)}
      />
    ];

    return (
      <div
        style={{
          height: '100%',
          width: '100%',
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
          textAlign: 'center'
        }}
      >
        <Dialog
          title="送信しますか？"
          actions={actions}
          modal
          open={this.state.open}
        >
          内容を確認した上で送信します。<br />
          To: {this.state.To} <br />
          From: {this.state.From} <br />
          MediaUrl: {this.state.MediaUrl} <br />
        </Dialog>
        <Dialog
          title="送信中"
          modal
          open={this.state.status}
        >
          <CircularProgress />
          <h2>{this.state.status}</h2>
        </Dialog>
        <div>
          <h1> Programmable Fax </h1>
          <Paper
            zDepth={2}
            rounded={false}
          >
            <div style={{ padding: '40px' }}>
              <h2> Fax Information </h2>
              <TextField fullWidth floatingLabelText="Fax URL" defaultValue={this.state.MediaUrl} value={this.state.MediaUrl} onChange={this.updateField.bind(this, 'MediaUrl')} />
              <TextField fullWidth floatingLabelText="To Number" value={this.state.To} onChange={this.updateField.bind(this, 'To')} />
              <TextField fullWidth floatingLabelText="From Number" value={this.state.From} onChange={this.updateField.bind(this, 'From')} />
              <h2> Basic Auth </h2>
              <TextField fullWidth floatingLabelText="User Name" value={this.state.AccountSID} onChange={this.updateField.bind(this, 'AccountSID')} />
              <TextField fullWidth floatingLabelText="Api Token" type="password" value={this.state.Token} onChange={this.updateField.bind(this, 'Token')} />
              <RaisedButton backgroundColor={red500} label="Submit" fullWidth onClick={this.submit.bind(this)} />
            </div>
          </Paper>
        </div>
      </div>
    );
  }
}
